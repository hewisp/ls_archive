/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_no_flags.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/02 20:34:06 by hewisp            #+#    #+#             */
/*   Updated: 2019/04/16 15:17:31 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

void			ft_print_colums(t_list_ls *b, unsigned short counter_colums
	, unsigned short height_colums, t_length *length)
{
	t_list_ls		*d;
	unsigned short	counter1;
	unsigned short	counter2;

	counter1 = 0;
	counter2 = 0;
	d = b;
	while (counter1 < counter_colums)
	{
		if (d)
			ft_print_array_i_space(length->max_length_nm + 1
				, d->length_name, d->file_name);
		else
			break ;
		while (counter2 < height_colums && d)
		{
			counter2++;
			d = d->next;
		}
		counter2 = 0;
		counter1++;
	}
}

unsigned short	ft_number_colums(t_length *length)
{
	struct winsize	size;
	unsigned short	counter_colums;

	ioctl(0, TIOCGWINSZ, &size);
	if (length->max_length_nm == 0)
	{
		return (1);
	}
	counter_colums = size.ws_col / (length->max_length_nm + 1);
	if (size.ws_col % length->max_length_nm > 0)
		counter_colums++;
	return (counter_colums);
}

unsigned short	ft_height_colums(t_length *length
	, unsigned short counter_colums)
{
	unsigned short	height_colums;

	if (counter_colums == 0)
		counter_colums = 1;
	if ((height_colums =
		(unsigned short)length->link_counter / counter_colums) == 0)
		height_colums++;
	else if (length->link_counter / counter_colums > 0 && counter_colums != 1)
		height_colums++;
	return (height_colums);
}

void			ft_print_all(t_list_ls *a, unsigned short counter_colums
	, unsigned short height_colums, t_length *length)
{
	unsigned short		counter;
	t_list_ls			*b;

	counter = 0;
	b = (t_list_ls*)a;
	while (height_colums > counter && b)
	{
		ft_print_colums(b, counter_colums, height_colums, length);
		write(1, "\n", 1);
		b = b->next;
		counter++;
	}
}

void			ft_print_no_flags(t_list_ls *a, t_length *length)
{
	unsigned short	counter_colums;
	unsigned short	height_colums;

	length->max_length_nm = 0;
	length->link_counter = 0;
	ft_list_len(a, length);
	counter_colums = ft_number_colums(length);
	if (counter_colums > 1)
		counter_colums--;
	height_colums = ft_height_colums(length, counter_colums);
	ft_print_all(a, counter_colums, height_colums, length);
}
