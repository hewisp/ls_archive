/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/02 21:08:47 by hewisp            #+#    #+#             */
/*   Updated: 2019/04/04 19:44:59 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

void	ft_sort_r(t_list_ls **a)
{
	t_list_ls	*pointer_back;
	t_list_ls	*pointer_next;
	t_list_ls	*pointer_buffer;

	pointer_buffer = *a;
	pointer_back = NULL;
	while (pointer_buffer->next)
	{
		pointer_next = pointer_buffer->next;
		pointer_buffer->next = pointer_back;
		pointer_back = pointer_buffer;
		pointer_buffer = pointer_next;
	}
	pointer_buffer->next = pointer_back;
	(*a) = pointer_buffer;
}

void	ft_insert_begin_list(t_list_ls *prev_list, t_list_ls *list
	, t_list_ls **first_list)
{
	prev_list = list->next->next;
	list->next->next = list;
	*first_list = list->next;
	list->next = prev_list;
}

void	ft_sort_list_t(t_list_ls **first_list)
{
	t_list_ls	*prev_list;
	t_list_ls	*list;

	prev_list = NULL;
	list = *first_list;
	while (list->next)
		if (list->system_mtime < list->next->system_mtime)
		{
			if (!prev_list)
				ft_insert_begin_list(prev_list, list, first_list);
			else
			{
				prev_list->next = list->next;
				list->next = list->next->next;
				prev_list->next->next = list;
			}
			prev_list = NULL;
			list = *first_list;
		}
		else
		{
			prev_list = list;
			list = list->next;
		}
}

void	ft_sort_list_ut(t_list_ls **first_list)
{
	t_list_ls	*prev_list;
	t_list_ls	*list;

	prev_list = NULL;
	list = *first_list;
	while (list->next)
		if (list->system_atime < list->next->system_atime)
		{
			if (!prev_list)
				ft_insert_begin_list(prev_list, list, first_list);
			else
			{
				prev_list->next = list->next;
				list->next = list->next->next;
				prev_list->next->next = list;
			}
			prev_list = NULL;
			list = *first_list;
		}
		else
		{
			prev_list = list;
			list = list->next;
		}
}

void	ft_skips_hidden_files(t_list_ls **a, t_length *length)
{
	t_list_ls *buf;
	t_list_ls *back;
	t_list_ls *free_list;

	back = NULL;
	buf = (*a);
	while (buf)
		if (buf->file_name[0] == '.')
		{
			if (back == NULL)
				(*a) = buf->next;
			else
				back->next = buf->next;
			free_list = buf;
			buf = buf->next;
			free_list->next = NULL;
			ft_free_list(&free_list, length);
		}
		else
		{
			back = buf;
			buf = buf->next;
		}
}
