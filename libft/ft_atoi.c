/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/24 16:45:07 by hewisp            #+#    #+#             */
/*   Updated: 2018/12/09 19:51:13 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_atoi(const char *str)
{
	size_t			index;
	long long int	a;
	long long int	b;
	int				sign;

	a = 0;
	sign = 1;
	index = 0;
	while (str[index] == '\v' || str[index] == '\f' || str[index] == '\t'
			|| str[index] == '\r' || str[index] == '\n' || str[index] == ' ')
		index++;
	if (str[index] == '-' || str[index] == '+')
	{
		if (str[index] == '-')
			sign = -1;
		index++;
	}
	while (str[index] >= '0' && str[index] <= '9')
	{
		b = a;
		a = a * 10 + ((sign) * (str[index++] - '0'));
		if ((a > 0 && b < 0) || (a < 0 && b > 0))
			return ((a > 0 && b < 0) ? 0 : -1);
	}
	return ((int)(a));
}
