/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/28 20:58:48 by hewisp            #+#    #+#             */
/*   Updated: 2018/12/13 19:03:11 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *destptr, const void *srcptr, size_t num)
{
	unsigned char		*a;
	unsigned const char *b;
	size_t				i;

	a = (unsigned char*)destptr;
	b = (unsigned const char*)srcptr;
	if (num == 0 || destptr == srcptr)
		return (destptr);
	i = 0;
	while (i < num)
	{
		a[i] = b[i];
		i++;
	}
	return (destptr);
}
