/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/30 13:30:20 by hewisp            #+#    #+#             */
/*   Updated: 2018/12/13 18:53:20 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strchr(const char *str, int ch)
{
	size_t	i;
	char	a;

	i = 0;
	a = (char)ch;
	while (str[i] && str[i] != (char)a)
		i++;
	if (str[i] == a)
		return ((char*)&str[i]);
	return (NULL);
}
