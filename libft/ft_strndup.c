/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strndup.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/13 16:59:07 by hewisp            #+#    #+#             */
/*   Updated: 2018/12/13 18:47:18 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strndup(const char *str, size_t i)
{
	char	*array;
	size_t	index;
	size_t	a;

	a = 0;
	if (i == a - 1)
		return (NULL);
	index = 0;
	array = (char*)malloc(sizeof(char) * (i + 1));
	if (array == NULL)
		return (NULL);
	while (index < i)
	{
		array[index] = str[index];
		index++;
	}
	array[index] = '\0';
	return (array);
}
