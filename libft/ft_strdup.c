/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/26 18:17:38 by hewisp            #+#    #+#             */
/*   Updated: 2018/12/10 19:03:13 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdup(const char *str)
{
	char	*array;
	size_t	length;
	size_t	index;

	length = 0;
	index = 0;
	while (str[length])
		length++;
	array = (char*)malloc(sizeof(char) * (length + 1));
	if (array == NULL)
		return (NULL);
	while (str[index])
	{
		array[index] = str[index];
		index++;
	}
	array[index] = '\0';
	return (array);
}
