/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/30 16:51:18 by hewisp            #+#    #+#             */
/*   Updated: 2019/02/24 12:20:05 by aleksei          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *memptr, int val, size_t num)
{
	size_t				i;
	unsigned char		ch;
	unsigned const char *p;

	i = 0;
	ch = (unsigned char)val;
	p = (unsigned const char*)memptr;
	while (i < num)
	{
		if (p[i] == ch)
			return ((void*)(memptr + i));
		i++;
	}
	return (NULL);
}
