/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/29 16:36:33 by hewisp            #+#    #+#             */
/*   Updated: 2018/12/15 16:47:36 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *destination, const void *source, size_t n)
{
	size_t				i;
	unsigned char		*p;
	unsigned const char	*s;

	i = 0;
	p = (unsigned char*)destination;
	s = (unsigned const char*)source;
	if (destination == source)
		return (destination);
	if (destination < source)
	{
		while (i < n)
		{
			p[i] = s[i];
			i++;
		}
	}
	else
	{
		while (n--)
			p[n] = s[n];
	}
	return (destination);
}
