/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 19:37:06 by hewisp            #+#    #+#             */
/*   Updated: 2018/12/12 21:26:12 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_sort(char **a, int n)
{
	size_t	index;
	char	*p;

	index = 1;
	while ((int)index < n)
	{
		if (ft_strcmp(a[index], a[index + 1]) > 0)
		{
			p = a[index];
			a[index] = a[index + 1];
			a[index + 1] = p;
			index--;
		}
		else
			index++;
	}
}
