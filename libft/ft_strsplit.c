/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/06 19:55:11 by hewisp            #+#    #+#             */
/*   Updated: 2018/12/13 16:26:03 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	ft_counter_words(char const *s, char c)
{
	size_t i;
	size_t word;

	i = 0;
	word = 0;
	while (s[i])
	{
		if ((s[i] != '\0' && s[i] != c) && (s[i + 1] == '\0' || s[i + 1] == c))
			word++;
		i++;
	}
	return (word);
}

static char		**sozdat_array(char const *s, char c)
{
	char	**p;
	size_t	nb;

	p = NULL;
	nb = ft_counter_words(s, c);
	p = (char**)malloc(sizeof(char *) * (nb + 1));
	if (p == NULL)
		return (NULL);
	p[nb] = NULL;
	return (p);
}

static size_t	ft_dlina_slova(char const *s, char c)
{
	size_t len;
	size_t i;

	len = 0;
	i = 0;
	while ((s[i] != c && s[i] != '\0'))
	{
		len++;
		i++;
	}
	return (len);
}

static char		**zabit_array(char const *s, char **source, char c)
{
	size_t i;
	size_t counter;

	i = 0;
	counter = 0;
	while (s[i])
		if (s[i] != c && s[i] != '\0')
		{
			source[counter++] = ft_strsub(s, i, ft_dlina_slova(&s[i], c));
			while (s[i] != c && s[i] != '\0')
				i++;
			if (source[counter - 1] == NULL)
			{
				ft_strdel_all_array(source, counter - 1);
				return (source);
			}
		}
		else
			i++;
	return (source);
}

char			**ft_strsplit(char const *source, char c)
{
	char **p;

	p = NULL;
	if (source)
	{
		p = sozdat_array(source, c);
		if (p == NULL)
			return (NULL);
		if (!zabit_array(source, p, c))
			return (NULL);
	}
	return (p);
}
