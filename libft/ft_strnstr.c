/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/29 20:25:03 by hewisp            #+#    #+#             */
/*   Updated: 2018/12/15 15:24:09 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *string1, const char *string2, size_t n)
{
	size_t	i1;
	size_t	i2;

	i1 = -1;
	i2 = 0;
	if (!string2[i2])
		return ((char*)string1);
	while (string1[++i1] && i1 < n)
	{
		if (string1[i1] == string2[i2])
		{
			while (string1[i1++] == string2[i2++] && i1 < n)
			{
				if (string2[i2] == '\0' || i1 > n)
					return ((char*)&(string1[i1 - i2]));
			}
			if (string2[i2] == '\0' && (string1[i1 - 1] == string2[i2 - 1]))
				return ((char*)&(string1[i1 - i2]));
			i1 = i1 - i2;
			i2 = 0;
		}
	}
	return (NULL);
}
