/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/05 18:46:12 by hewisp            #+#    #+#             */
/*   Updated: 2019/02/24 11:55:44 by aleksei          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strtrim(char const *s)
{
	size_t	start;
	size_t	end;
	char	*p;

	start = 0;
	end = 0;
	p = NULL;
	if (s)
	{
		while (s[start] == ' ' || s[start] == ',' || s[start] == '\n' ||
		s[start] == '\t')
			start++;
		end = ft_strlen(s) - 1;
		while ((end > start) && (s[end] == ' ' || s[end] == ',' ||
		s[end] == '\n' || s[end] == '\t'))
			end--;
		p = ft_strsub(s, start, (end - start + 1));
	}
	return (p);
}
