/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/05 17:59:54 by hewisp            #+#    #+#             */
/*   Updated: 2019/02/24 11:54:39 by aleksei          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	char	*p;

	p = NULL;
	if (s)
	{
		p = ft_strnew(len);
		if (p == NULL)
			return (NULL);
		ft_strncpy(p, &s[start], len);
	}
	return (p);
}
