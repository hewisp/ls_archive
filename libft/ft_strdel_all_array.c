/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdel_all_array.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/13 16:11:53 by hewisp            #+#    #+#             */
/*   Updated: 2018/12/13 17:22:55 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_strdel_all_array(char **a, size_t i)
{
	while (i != 0)
	{
		free(a[i]);
		i--;
	}
	free(a[i]);
	ft_strdel(a);
}
