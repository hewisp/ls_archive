/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/30 13:52:39 by hewisp            #+#    #+#             */
/*   Updated: 2018/12/13 19:12:36 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *string, int symbol)
{
	size_t			i;
	char			ch;

	i = ft_strlen(string);
	ch = (char)symbol;
	while (1)
	{
		if (string[i] == ch)
			return ((char*)&string[i]);
		if (i == 0)
			return (NULL);
		i--;
	}
	return (NULL);
}
