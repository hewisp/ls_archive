/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/08 19:17:03 by hewisp            #+#    #+#             */
/*   Updated: 2018/12/09 19:51:33 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstdel(t_list **alst, void (*del)(void*, size_t))
{
	t_list *p;
	t_list *next_p;

	p = (*alst);
	if (*alst && del)
	{
		while (p)
		{
			next_p = p->next;
			del(p->content, p->content_size);
			free(p);
			p = next_p;
		}
		(*alst) = NULL;
	}
}
