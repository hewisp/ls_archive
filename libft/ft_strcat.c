/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/27 19:47:36 by hewisp            #+#    #+#             */
/*   Updated: 2018/12/09 19:49:03 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strcat(char *destination, const char *append)
{
	size_t i;
	size_t i2;

	i = 0;
	i2 = 0;
	while (destination[i])
	{
		i++;
	}
	while (append[i2])
	{
		destination[i] = append[i2];
		i++;
		i2++;
	}
	destination[i] = '\0';
	return (destination);
}
