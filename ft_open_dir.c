/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_open_dir.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/02 18:21:46 by hewisp            #+#    #+#             */
/*   Updated: 2019/04/05 19:20:20 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

int			ft_insert_list_in_while(t_list_ls **new_list, t_list_ls **first_list
	, t_list_ls **prev_list, t_list_ls **list)
{
	if (ft_strcmp((*new_list)->file_name, (*list)->file_name) < 0)
	{
		if (!(*prev_list))
		{
			(*new_list)->next = *list;
			*first_list = (*new_list);
			return (1);
		}
		(*new_list)->next = *list;
		(*prev_list)->next = (*new_list);
		return (1);
	}
	*prev_list = *list;
	*list = (*list)->next;
	return (0);
}

void		ft_insert_list(t_list_ls **new_list, t_list_ls **first_list
	, t_list_ls **prev_list, t_list_ls **list)
{
	if (ft_strcmp((*new_list)->file_name, (*list)->file_name) < 0)
	{
		(*new_list)->next = (*list);
		if (!(*prev_list))
			*first_list = (*new_list);
		else
			(*prev_list)->next = (*new_list);
	}
	else
	{
		(*list)->next = (*new_list);
		(*new_list)->next = NULL;
	}
}

t_list_ls	*ft_sort_and_allocate_list(t_list_ls **first_list
	, char *name, char *arg)
{
	t_list_ls *new_list;
	t_list_ls *prev_list;
	t_list_ls *list;

	prev_list = NULL;
	if (!(new_list = (t_list_ls*)malloc(sizeof(t_list_ls))) ||
	!(new_list->length_name = ft_strlen(name)) ||
	!(new_list->file_name = ft_strndup(name, new_list->length_name)))
	{
		perror("ft_ls");
		exit(0);
	}
	new_list->check_error = 0;
	new_list->next = NULL;
	ft_full_way_to_file(arg, &new_list);
	if (!(*first_list))
		return (*first_list = new_list);
	list = *first_list;
	while (list->next)
		if (ft_insert_list_in_while(&new_list, first_list, &prev_list
			, &list) == 1)
			return (*first_list);
	ft_insert_list(&new_list, first_list, &prev_list, &list);
	return (*first_list);
}

t_list_ls	*ft_add_list_to_end(t_list_ls **first, char *name, char *arg)
{
	t_list_ls *list;
	t_list_ls *new_list;

	list = (*first);
	if (!(new_list = (t_list_ls*)malloc(sizeof(t_list_ls))) ||
	!(new_list->length_name = ft_strlen(name)) ||
	!(new_list->file_name = ft_strndup(name, new_list->length_name)))
	{
		perror("ft_ls");
		exit(0);
	}
	new_list->check_error = 0;
	ft_full_way_to_file(arg, &new_list);
	new_list->next = NULL;
	if (!(*first))
		return ((*first) = new_list);
	while (list->next)
		list = list->next;
	list->next = new_list;
	return (*first);
}

t_list_ls	*ft_open_dir(char *arg, t_length *length)
{
	DIR				*a;
	t_list_ls		*b;
	struct dirent	*entry;
	char			*next;
	t_list_ls		*first_list;

	next = NULL;
	first_list = NULL;
	b = NULL;
	if (!(a = opendir(arg)))
	{
		perror("error");
		return (NULL);
	}
	while ((entry = readdir(a)))
		if (length->flag_f)
			ft_add_list_to_end(&first_list, entry->d_name, arg);
		else
			first_list = ft_sort_and_allocate_list(&first_list
				, entry->d_name, arg);
	closedir(a);
	return (first_list);
}
