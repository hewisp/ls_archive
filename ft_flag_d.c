/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_flag_d.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/04 16:31:31 by hewisp            #+#    #+#             */
/*   Updated: 2019/04/06 14:33:14 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

void		ft_flag_d_2(int argc, char **argv, t_length *length
	, t_list_ls **first)
{
	int i;

	i = 1;
	while (i < argc)
	{
		if (argv[i])
		{
			if (length->flag_f)
				ft_add_list_to_end(first, argv[i], "./");
			else
				(*first) = ft_sort_and_allocate_list(first, argv[i], "./");
		}
		i++;
	}
}

t_list_ls	*ft_flag_d(int argc, char **argv, t_length *length)
{
	t_list			*b;
	char			*next;
	t_list_ls		*first_list;

	next = NULL;
	first_list = NULL;
	b = NULL;
	length->flag_a = 1;
	ft_flag_d_2(argc, argv, length, &first_list);
	if (length->flag_l || length->flag_u || length->flag_t || length->flag_bu)
		ft_lstiter_a_list(&first_list, length, &ft_filling_structure);
	ft_apply_flag(&first_list, length);
	if (length->flag_1)
		ft_flag_1(first_list, length);
	else if (length->flag_l)
		ft_lstiter_a_list(&first_list, length, &ft_print_arg);
	else
		ft_print_no_flags(first_list, length);
	ft_free_list(&first_list, length);
	return (first_list);
}
