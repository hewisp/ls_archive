/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parcing.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/02 20:05:29 by hewisp            #+#    #+#             */
/*   Updated: 2019/04/16 16:47:58 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

void	ft_annihilate_length(t_length **length)
{
	(*length)->flag_l = 0;
	(*length)->flag_a = 0;
	(*length)->flag_r = 0;
	(*length)->flag_t = 0;
	(*length)->flag_br = 0;
	(*length)->flag_g = 0;
	(*length)->flag_d = 0;
	(*length)->flag_u = 0;
	(*length)->flag_f = 0;
	(*length)->flag_1 = 0;
	(*length)->flag_bu = 0;
}

int		ft_check_1(char **argv, size_t num_arg, size_t i, t_length *length)
{
	if (argv[num_arg][i] == '1')
	{
		length->flag_1 = 1;
		length->flag_l = 0;
	}
	else if (argv[num_arg][i] == 'l')
	{
		length->flag_l = 1;
		length->flag_1 = 0;
	}
	else if (argv[num_arg][i] == 'r')
		length->flag_r = 1;
	else if (argv[num_arg][i] == 'a')
		length->flag_a = 1;
	else if (argv[num_arg][i] == 't')
		length->flag_t = 1;
	else if (argv[num_arg][i] == 'R')
		length->flag_br = 1;
	else
		return (0);
	return (1);
}

void	ft_check_2(char **argv, size_t num_arg, size_t i, t_length *length)
{
	if (argv[num_arg][i] == 'g')
	{
		length->flag_g = 1;
		length->flag_l = 1;
	}
	else if (argv[num_arg][i] == 'd')
	{
		length->flag_d = 1;
		length->flag_a = 1;
	}
	else if (argv[num_arg][i] == 'u')
		length->flag_u = 1;
	else if (argv[num_arg][i] == 'f')
		length->flag_f = 1;
	else if (argv[num_arg][i] == 'U')
		length->flag_bu = 1;
	else
	{
		write(1, "ft_ls: illegal option -- ", 25);
		write(1, &argv[num_arg][i], 1);
		write(1, "\nusage: ft_ls [-lRartuUfgd1] [file ...]\n", 40);
		exit(0);
	}
}

void	ft_check_arg_parcing(char **argv, size_t num_arg, size_t i
	, t_length *length)
{
	while (argv[num_arg][i])
	{
		if (!ft_check_1(argv, num_arg, i, length))
			ft_check_2(argv, num_arg, i, length);
		i++;
	}
}

char	*ft_parcing(int argc, char **argv, t_length *length)
{
	int		num_arg;
	size_t	i;

	i = 1;
	num_arg = 1;
	ft_annihilate_length(&length);
	while (num_arg < argc)
	{
		if (argv[num_arg][0] == '-' && argv[num_arg][1])
		{
			ft_check_arg_parcing(argv, num_arg, i, length);
			argv[num_arg] = NULL;
			i = 1;
		}
		num_arg++;
	}
	if (length->flag_f)
	{
		length->flag_a = 1;
		length->flag_t = 0;
		length->flag_r = 0;
	}
	if (length->flag_d)
		length->flag_br = 0;
	return (NULL);
}
