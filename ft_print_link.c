/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_link.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/04 12:37:57 by hewisp            #+#    #+#             */
/*   Updated: 2019/04/06 14:40:48 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

void	ft_print_link_3(t_list_ls **first, t_length *length, char name_dir)
{
	while (*first)
	{
		if (length->flag_l || length->flag_u || length->flag_t
			|| length->flag_bu)
			ft_lstiter_a_list(first, length, &ft_filling_structure);
		if (name_dir)
		{
			write(1, (*first)->file_name, (*first)->length_name);
			write(1, ":\n", 2);
		}
		ft_flag_r(length, (*first)->file_name);
		(*first) = (*first)->next;
		if (*first)
			write(1, "\n", 1);
	}
}

void	ft_print_link_2(t_list_ls **first, t_length *length)
{
	if (length->flag_1)
		ft_flag_1((*first), length);
	else if (length->flag_l)
	{
		ft_lstiter_a_list(first, length, &ft_filling_structure);
		ft_lstiter_a_list(first, length, &ft_print_arg);
	}
	else
		ft_print_no_flags(*first, length);
}

void	ft_print_link_1(char **argv, t_list_ls **first, int argc)
{
	int			i;
	struct stat	buf;

	i = 1;
	while (i < argc)
	{
		if (argv[i])
		{
			lstat(argv[i], &buf);
			if (ft_file_type(buf.st_mode) == 'l')
			{
				(*first) = ft_sort_and_allocate_list(first, argv[i], "./");
				argv[i] = NULL;
			}
		}
		i++;
	}
}

void	ft_print_link(int argc, char **argv, t_length *length)
{
	t_list_ls		*first;
	t_list_ls		*list;
	char			name_dir;

	first = NULL;
	name_dir = 0;
	ft_print_link_1(argv, &first, argc);
	if (first)
	{
		list = first;
		ft_apply_flag(&first, length);
		if (first->next)
			name_dir = 1;
		if ((length->flag_l) || (length->flag_d))
			ft_print_link_2(&first, length);
		else
			ft_print_link_3(&first, length, name_dir);
		ft_free_list(&list, length);
	}
}
