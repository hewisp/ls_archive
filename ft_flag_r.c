/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_flag_r.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/03 12:44:23 by hewisp            #+#    #+#             */
/*   Updated: 2019/04/11 20:07:12 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

void	ft_anihilate_length(t_length *length, t_list_ls **first, char *arg)
{
	length->max_length_pw = 0;
	length->max_length_gr = 0;
	length->max_length_nm = 0;
	length->max_link_caunt_size = 0;
	length->max_length_size = 0;
	length->check_acl = 0;
	if (!((*first) = ft_open_dir(arg, length)))
		return ;
	if (length->flag_l || length->flag_t || length->flag_bu || length->flag_br)
		ft_lstiter_a_list(&(*first), length, &ft_filling_structure);
	ft_apply_flag(&(*first), length);
	if (length->flag_1)
		ft_flag_1((*first), length);
	else if (length->flag_l)
	{
		if ((*first) && !(*first)->check_error)
		{
			write(1, "total ", 6);
			ft_putnbr(length->total_size);
			write(1, "\n", 1);
		}
		ft_lstiter_a_list(&(*first), length, &ft_print_arg);
	}
	else
		ft_print_no_flags((*first), length);
}

void	ft_flag_r(t_length *length, char *arg)
{
	t_list_ls *first;
	t_list_ls *list;

	length->check_major = 0;
	first = NULL;
	ft_anihilate_length(length, &first, arg);
	list = first;
	if (length->flag_br)
		while (list)
		{
			length->total_size = 0;
			if (!(list->check_error) && (list->access_right[0] == 'd'
			&& !((list->file_name[0] == '.' && list->file_name[1] == '\0')
			|| (list->file_name[0] == '.' && list->file_name[1] == '.'
			&& list->file_name[2] == '\0'))))
			{
				write(1, "\n", 1);
				write(1, list->full_way_to_file
					, ft_strlen(list->full_way_to_file));
				write(1, ":\n", 2);
				ft_flag_r(length, list->full_way_to_file);
			}
			list = list->next;
		}
	ft_free_list(&first, length);
}
