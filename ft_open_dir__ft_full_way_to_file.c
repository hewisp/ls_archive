/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_open_dir__ft_full_way_to_file.c                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/02 20:01:36 by hewisp            #+#    #+#             */
/*   Updated: 2019/04/05 19:05:03 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

void	ft_full_way_to_file(char *arg, t_list_ls **list)
{
	size_t j;
	size_t z;
	size_t i;

	j = 0;
	z = 0;
	i = (*list)->length_name + ft_strlen(arg);
	if (!((*list)->full_way_to_file = (char*)malloc(sizeof(char) * (i + 2))))
	{
		perror("ls");
		exit(0);
	}
	if ((*list)->file_name[0] != '/')
	{
		while (arg[j])
		{
			(*list)->full_way_to_file[j] = arg[j];
			j++;
		}
		if ((*list)->full_way_to_file[j - 1] != '/')
			(*list)->full_way_to_file[j++] = '/';
	}
	while ((*list)->file_name[z])
		(*list)->full_way_to_file[j++] = (*list)->file_name[z++];
	(*list)->full_way_to_file[j] = '\0';
}

void	ft_sort_list_but(t_list_ls **first_list)
{
	t_list_ls	*prev_list;
	t_list_ls	*list;

	prev_list = NULL;
	list = *first_list;
	while (list->next)
		if (list->system_ctime < list->next->system_ctime)
		{
			if (!prev_list)
				ft_insert_begin_list(prev_list, list, first_list);
			else
			{
				prev_list->next = list->next;
				list->next = list->next->next;
				prev_list->next->next = list;
			}
			prev_list = NULL;
			list = *first_list;
		}
		else
		{
			prev_list = list;
			list = list->next;
		}
}
