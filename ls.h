/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/04 16:57:40 by hewisp            #+#    #+#             */
/*   Updated: 2019/04/05 19:18:50 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LS_H
# define LS_H
# include <dirent.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <time.h>
# include <pwd.h>
# include <stdlib.h>
# include <grp.h>
# include <stdio.h>
# include "libft.h"
# include <sys/ioctl.h>
# include <sys/xattr.h>
# include <sys/acl.h>

typedef struct			s_list_ls
{
	struct s_list_ls	*next;
	char				*access_right;
	char				*file_name;
	char				*full_way_to_file;
	char				*last_file_mod;
	char				*pw_name;
	char				*gr_name;
	char				*link;
	char				check_error;
	size_t				major;
	size_t				minor;
	size_t				link_length;
	size_t				system_mtime;
	size_t				system_atime;
	size_t				system_ctime;
	size_t				st_size;
	size_t				size;
	size_t				length_size;
	size_t				length_name;
	size_t				length_pw;
	size_t				length_gr;
	size_t				link_caunt;
	size_t				link_caunt_size;
	size_t				size_major;
	size_t				size_minor;
}						t_list_ls;

typedef	struct			s_length
{
	size_t				max_length_pw;
	size_t				max_length_gr;
	size_t				max_length_nm;
	size_t				max_length_size;
	size_t				max_link_caunt_size;
	size_t				link_counter;
	size_t				total_size;
	size_t				max_size_major;
	size_t				max_size_minor;
	char				check_acl;
	char				check_major;
	char				flag_1;
	char				flag_f;
	char				flag_l;
	char				flag_a;
	char				flag_r;
	char				flag_t;
	char				flag_br;
	char				flag_g;
	char				flag_d;
	char				flag_u;
	char				flag_bu;
}						t_length;

char					ft_file_type(mode_t arg);
void					ft_acces_right(t_list_ls **a, struct stat *buf
	, t_length *length);
void					ft_filling_structure(t_list_ls **a, t_length *length);
t_list_ls				*ft_open_dir(char *arg, t_length *length);
t_list_ls				*ft_add_list_to_end(t_list_ls **first
	, char *name, char *arg);
t_list_ls				*ft_sort_and_allocate_list(t_list_ls **first_list
	, char *name, char *arg);
void					ft_full_way_to_file(char *arg, t_list_ls **list);
char					*ft_parcing(int argc, char **argv, t_length *length);
void					ft_print_no_flags(t_list_ls *a, t_length *length);
void					ft_print_colums(t_list_ls *b
	, unsigned short counter_colums
	, unsigned short height_colums, t_length *length);
void					ft_list_len(t_list_ls *a, t_length *length);
void					ft_print_array_i_space(size_t all_length
	, size_t length_array, char *array);
void					ft_print_all(t_list_ls *a, unsigned short counter_colums
	, unsigned short height_colums, t_length *length);
void					ft_print_acess_right(t_list_ls **a, t_length *length);
void					ft_print_space_i_array(size_t all_length
	, size_t length_array, char *array);
void					ft_print_array_i_space(size_t all_length
	, size_t length_array, char *array);
void					ft_print_space_i_num(size_t max, size_t num_counter
	, size_t number);
void					ft_sort_r(t_list_ls **a);
void					ft_sort_list_t(t_list_ls **first_list);
void					ft_sort_list_ut(t_list_ls **first_list);
void					ft_sort_list_but(t_list_ls **first_list);
void					ft_insert_begin_list(t_list_ls *prev_list
	, t_list_ls *list, t_list_ls **first_list);
void					ft_print_arg(t_list_ls **a, t_length *length);
void					ft_flag_1(t_list_ls *first_list, t_length *length);
void					ft_skips_hidden_files(t_list_ls **a, t_length *length);
void					ft_list_len(t_list_ls *a, t_length *length);
void					ft_lstiter_a_list(t_list_ls **first_list
	, t_length *length, void (*f)(t_list_ls **l, t_length *le));
void					ft_free_list(t_list_ls **first_list, t_length *length);
void					ft_apply_flag(t_list_ls **first, t_length *length);
void					ft_flag_r(t_length *length, char *arg);
int						ft_print_file(int argc, char **argv, t_length *length);
void					ft_print_dir(int argc, char **argv, t_length *length
	, int enter);
void					ft_print_link(int argc, char **argv, t_length *length);
t_list_ls				*ft_flag_d(int argc, char **argv, t_length *length);

#endif
