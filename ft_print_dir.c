/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_dir.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/03 13:53:35 by hewisp            #+#    #+#             */
/*   Updated: 2019/04/06 14:38:23 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

void	ft_print_dir_5(t_list_ls *first)
{
	write(1, first->file_name, first->length_name);
	write(1, ":\n", 2);
}

void	ft_print_dir_4(t_list_ls **list, t_list_ls **first)
{
	while ((*list))
	{
		free((*list)->file_name);
		free((*list)->full_way_to_file);
		(*first) = (*list);
		(*list) = (*list)->next;
		free((*first));
	}
}

void	ft_print_dir_3(char **argv, t_list_ls **first, int argc)
{
	int			i;
	struct stat	buf;

	i = 1;
	while (i < argc)
	{
		if (argv[i])
			lstat(argv[i], &buf);
		if (argv[i] && (ft_file_type(buf.st_mode) == 'd'))
		{
			(*first) = ft_sort_and_allocate_list(first, argv[i], "./");
			argv[i] = NULL;
		}
		i++;
	}
}

void	ft_print_dir_2(t_list_ls **first, t_length *length
	, t_list_ls **list)
{
	if (length->flag_d)
	{
		ft_lstiter_a_list(first, length, &ft_filling_structure);
		ft_apply_flag(first, length);
		if (length->flag_1)
			ft_flag_1((*first), length);
		if (length->flag_l)
			ft_lstiter_a_list(first, length, &ft_print_arg);
		else
			ft_print_no_flags((*first), length);
		ft_free_list(list, length);
		return ;
	}
	ft_flag_r(length, (*first)->file_name);
	(*first) = (*first)->next;
	if ((*first))
		write(1, "\n", 1);
}

void	ft_print_dir(int argc, char **argv, t_length *length, int enter)
{
	t_list_ls	*first;
	t_list_ls	*list;
	char		name_dir;

	first = NULL;
	list = NULL;
	name_dir = 0;
	ft_print_dir_3(argv, &first, argc);
	if (first && first->next)
		name_dir = 1;
	list = first;
	if (first)
	{
		if (enter)
			write(1, "\n", 1);
		while (first)
		{
			if ((name_dir && !length->flag_d) || enter)
				ft_print_dir_5(first);
			ft_print_dir_2(&first, length, &list);
		}
		ft_print_dir_4(&list, &first);
	}
}
