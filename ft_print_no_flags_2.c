/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_no_flags_2.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/02 20:40:06 by hewisp            #+#    #+#             */
/*   Updated: 2019/04/05 20:40:16 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

void	ft_print_space_i_num(size_t max, size_t num_counter, size_t number)
{
	size_t i;

	i = 0;
	while (i < max - num_counter)
	{
		write(1, " ", 1);
		i++;
	}
	ft_putnbr(number);
}

void	ft_print_acess_right(t_list_ls **a, t_length *length)
{
	write(1, (*a)->access_right, 10);
	if ((*a)->access_right[10])
	{
		write(1, &(*a)->access_right[10], 1);
		write(1, " ", 1);
	}
	else if (length->check_acl == 1)
		write(1, "  ", 2);
	else
		write(1, "  ", 2);
}

void	ft_print_space_i_array(size_t all_length, size_t length_array
	, char *array)
{
	size_t i;

	i = all_length - length_array;
	write(1, array, length_array);
	while (i--)
		write(1, " ", 1);
}

void	ft_print_array_i_space(size_t all_length, size_t length_array
	, char *array)
{
	size_t i;

	i = all_length - length_array;
	write(1, array, length_array);
	while (i--)
		write(1, " ", 1);
}
