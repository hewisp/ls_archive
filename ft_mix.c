/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mix.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/02 21:37:32 by hewisp            #+#    #+#             */
/*   Updated: 2019/04/16 16:24:51 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

void	ft_list_len(t_list_ls *a, t_length *length)
{
	t_list_ls *list;

	list = a;
	while (list)
	{
		if ((list->length_name) > length->max_length_nm)
			length->max_length_nm = list->length_name;
		length->link_counter++;
		list = list->next;
	}
}

void	ft_lstiter_a_list(t_list_ls **first_list, t_length *length
	, void (*f)(t_list_ls **l, t_length *le))
{
	t_list_ls *p;

	p = *first_list;
	while (p)
	{
		f(&p, length);
		p = p->next;
	}
}

void	ft_free_list(t_list_ls **first_list, t_length *length)
{
	t_list_ls *list;

	while ((*first_list))
	{
		list = *first_list;
		free((*first_list)->file_name);
		free((*first_list)->full_way_to_file);
		if (!(*first_list)->check_error && (length->flag_l || length->flag_br
			|| length->flag_t))
		{
			free((*first_list)->access_right);
			free((*first_list)->last_file_mod);
			free((*first_list)->pw_name);
			free((*first_list)->gr_name);
			if ((*first_list)->link)
				free((*first_list)->link);
		}
		(*first_list) = (*first_list)->next;
		free(list);
	}
}

void	ft_apply_flag(t_list_ls **first, t_length *length)
{
	if (!(length->flag_a))
		ft_skips_hidden_files(first, length);
	if ((*first) && (*first)->next)
	{
		if (length->flag_t && length->flag_bu)
		{
			ft_sort_list_but(first);
		}
		else if (length->flag_t && length->flag_u)
		{
			ft_sort_list_ut(first);
		}
		else
		{
			if (length->flag_t)
			{
				ft_sort_list_t(first);
			}
		}
		if (length->flag_r)
			ft_sort_r(first);
	}
}
