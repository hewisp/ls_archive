/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_file.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/03 13:31:46 by hewisp            #+#    #+#             */
/*   Updated: 2019/04/05 19:14:12 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

void	ft_print_file_2(int argc, char **argv, t_list_ls **first)
{
	int			i;
	struct stat	buf;

	i = 1;
	while (i < argc)
	{
		if (argv[i] && ((lstat(argv[i], &buf)) == -1))
		{
			write(1, "ft_ls: ", 7);
			write(1, argv[i], ft_strlen(argv[i]));
			write(1, ": ", 2);
			perror("");
			argv[i] = NULL;
			i = 1;
			continue;
		}
		else if (argv[i] && (ft_file_type(buf.st_mode) != 'd')
		&& (ft_file_type(buf.st_mode) != 'l'))
		{
			(*first) = ft_sort_and_allocate_list(first, argv[i], "./");
			argv[i] = NULL;
		}
		i++;
	}
}

int		ft_print_file(int argc, char **argv, t_length *length)
{
	int			i;
	t_list_ls	*first;

	i = 1;
	first = NULL;
	ft_print_file_2(argc, argv, &first);
	if (first)
	{
		if (length->flag_l || length->flag_t || length->flag_bu)
			ft_lstiter_a_list(&first, length, &ft_filling_structure);
		ft_apply_flag(&first, length);
		if (length->flag_1)
			ft_flag_1(first, length);
		else if (length->flag_l)
		{
			ft_lstiter_a_list(&first, length, &ft_print_arg);
		}
		else
			ft_print_no_flags(first, length);
		ft_free_list(&first, length);
		return (1);
	}
	return (0);
}
