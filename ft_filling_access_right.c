/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_filling_access_right.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/02 18:15:10 by hewisp            #+#    #+#             */
/*   Updated: 2019/04/11 20:30:00 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

char	ft_file_type(mode_t arg)
{
	if (S_ISREG(arg))
		return ('-');
	if (S_ISDIR(arg))
		return ('d');
	if (S_ISCHR(arg))
		return ('c');
	if (S_ISBLK(arg))
		return ('b');
	if (S_ISLNK(arg))
		return ('l');
	if (S_ISSOCK(arg))
		return ('s');
	if (S_ISFIFO(arg))
		return ('p');
	return ('0');
}

void	ft_atribute(t_list_ls **a, struct stat *buf, t_length *length)
{
	void	*acl;

	acl = NULL;
	if (S_ISUID & buf->st_mode)
		(*a)->access_right[3] = (*a)->access_right[3] == '-' ? 'S' : 's';
	if (S_ISGID & buf->st_mode)
		(*a)->access_right[6] = (*a)->access_right[6] == '-' ? 'S' : 's';
	if (S_ISVTX & buf->st_mode)
		(*a)->access_right[9] = (*a)->access_right[9] == '-' ? 'T' : 't';
	if (listxattr((*a)->full_way_to_file, NULL, 0, XATTR_NOFOLLOW) > 0)
	{
		(*a)->access_right[10] = '@';
		length->check_acl = 1;
	}
	else if ((acl = (void*)acl_get_link_np((*a)->full_way_to_file
	, ACL_TYPE_EXTENDED)))
	{
		(*a)->access_right[10] = '+';
		length->check_acl = 1;
	}
	else
		(*a)->access_right[10] = '\0';
	(*a)->access_right[11] = '\0';
	free(acl);
}

void	ft_acces_right(t_list_ls **a, struct stat *buf, t_length *length)
{
	if (!((*a)->access_right = (char*)malloc(sizeof(char) * 12)))
	{
		perror("malloc does not work");
	}
	if (((*a)->access_right[0] = ft_file_type(buf->st_mode)) == 'l')
	{
		(*a)->link = (char*)malloc(4096);
		(*a)->link_length = readlink((*a)->full_way_to_file, (*a)->link, 4096);
		free((*a)->link);
		(*a)->link = (char*)malloc((sizeof(char) * ((*a)->link_length + 1)));
		readlink((*a)->full_way_to_file, (*a)->link, (*a)->link_length);
		(*a)->link[(*a)->link_length] = '\0';
	}
	else
		(*a)->link = NULL;
	(*a)->access_right[1] = S_IRUSR & buf->st_mode ? 'r' : '-';
	(*a)->access_right[2] = S_IWUSR & buf->st_mode ? 'w' : '-';
	(*a)->access_right[3] = S_IXUSR & buf->st_mode ? 'x' : '-';
	(*a)->access_right[4] = S_IRGRP & buf->st_mode ? 'r' : '-';
	(*a)->access_right[5] = S_IWGRP & buf->st_mode ? 'w' : '-';
	(*a)->access_right[6] = S_IXGRP & buf->st_mode ? 'x' : '-';
	(*a)->access_right[7] = S_IROTH & buf->st_mode ? 'r' : '-';
	(*a)->access_right[8] = S_IWOTH & buf->st_mode ? 'w' : '-';
	(*a)->access_right[9] = S_IXOTH & buf->st_mode ? 'x' : '-';
	ft_atribute(a, buf, length);
}
