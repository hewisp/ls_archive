/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_filling.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/02 17:41:21 by hewisp            #+#    #+#             */
/*   Updated: 2019/04/05 20:52:40 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

size_t	ft_number_counter(size_t arg)
{
	size_t x;

	x = 0;
	if (arg == 0)
		return (1);
	while (arg != 0)
	{
		arg /= 10;
		x++;
	}
	return (x);
}

void	ft_filling_major_minor(t_list_ls **a, struct stat buf, t_length *length)
{
	(*a)->system_mtime = buf.st_mtime;
	ft_acces_right(a, &buf, length);
	if (((*a)->access_right[0] == 'c') || ((*a)->access_right[0] == 'b'))
	{
		length->check_major = 1;
		(*a)->major = major(buf.st_rdev);
		(*a)->minor = minor(buf.st_rdev);
		if (((*a)->size_major = ft_number_counter((*a)->major))
		> length->max_size_major)
			length->max_size_major = (*a)->size_major;
		if (((*a)->size_minor = ft_number_counter((*a)->minor))
		> length->max_size_minor)
			length->max_size_minor = (*a)->size_minor;
	}
	else
		(*a)->size = buf.st_size;
}

void	ft_filling_length(t_list_ls **a, struct stat buf, t_length *length)
{
	(*a)->system_atime = buf.st_atime;
	if (((*a)->file_name[0] == '.' && length->flag_a == 1)
		|| (*a)->file_name[0] != '.')
		length->total_size += buf.st_blocks;
	if (((*a)->length_pw = ft_strlen((getpwuid(buf.st_uid))->pw_name))
	> length->max_length_pw)
		length->max_length_pw = (*a)->length_pw;
	if (((*a)->length_gr = ft_strlen(getgrgid(buf.st_gid)->gr_name))
	> length->max_length_gr)
		length->max_length_gr = (*a)->length_gr;
	if (((*a)->length_name) > length->max_length_nm)
		length->max_length_nm = (*a)->length_name;
	if (((*a)->length_size = ft_number_counter(buf.st_size))
	> length->max_length_size)
		length->max_length_size = (*a)->length_size;
	(*a)->link_caunt = buf.st_nlink;
	if (((*a)->link_caunt_size = ft_number_counter(buf.st_nlink))
	> length->max_link_caunt_size)
		length->max_link_caunt_size = (*a)->link_caunt_size;
}

void	ft_filling_structure(t_list_ls **a, t_length *length)
{
	struct stat	buf;

	if (lstat((*a)->full_way_to_file, &buf) == -1)
	{
		(*a)->check_error = 1;
		return ;
	}
	if (!(*a))
		exit(0);
	ft_filling_length(a, buf, length);
	(*a)->system_ctime = buf.st_birthtime;
	ft_filling_major_minor(a, buf, length);
	if (length->check_major && length->flag_bu)
		(*a)->last_file_mod = ft_strndup(&ctime(&buf.st_birthtime)[20], 4);
	else if (length->flag_bu)
		(*a)->last_file_mod = ft_strndup(&ctime(&buf.st_birthtime)[4], 12);
	else if (length->flag_u)
		(*a)->last_file_mod = ft_strndup(&ctime(&buf.st_atime)[4], 12);
	else
		(*a)->last_file_mod = ft_strndup(&ctime(&buf.st_mtime)[4], 12);
	if (!((*a)->pw_name = ft_strndup(getpwuid(buf.st_uid)->pw_name,
	(*a)->length_pw))
	|| !((*a)->gr_name = ft_strndup(getgrgid(buf.st_gid)->gr_name,
	(*a)->length_gr)))
		perror("malloc does not work");
}
