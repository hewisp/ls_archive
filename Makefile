NAME = ft_ls
FILES = main.c ft_filling.c ft_filling_access_right.c ft_open_dir.c ft_open_dir__ft_full_way_to_file.c \
ft_parcing.c ft_print_no_flags.c ft_print_no_flags_2.c ft_sort.c ft_print_arg.c ft_mix.c ft_flag_r.c \
ft_print_file.c ft_print_dir.c ft_print_link.c ft_flag_d.c
OBJECTS = main.o ft_filling.o ft_filling_access_right.o	ft_open_dir.o ft_open_dir__ft_full_way_to_file.o \
ft_parcing.o ft_print_no_flags.o ft_print_no_flags_2.o ft_sort.o ft_print_arg.o ft_mix.o ft_flag_r.o \
ft_print_file.o ft_print_dir.o ft_print_link.o ft_flag_d.o
FLAGS = -Wall -Wextra -Werror

all: $(NAME)

$(NAME): $(OBJECTS)
	@gcc $(FLAGS) $(OBJECTS) -o $(NAME) -L./libft -lft -I ./libft/includes/ -g
	@echo "Все файлы собраны!"
$(OBJECTS):
	@gcc $(FLAGS) -c $(FILES) -I ./libft/includes/ -g
	@make -C ./libft/
	@echo "Объектные файлы собраны!"
clean:
	@rm -f $(OBJECTS)
	@make clean -C ./libft/
	@echo "Объектные файлы уничтожены!"
fclean: clean
	@rm -f $(NAME)
	@make fclean -C ./libft/
re: fclean all

test: re
	./ls 1
