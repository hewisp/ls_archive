/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_arg.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hewisp <hewisp@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/02 21:18:38 by hewisp            #+#    #+#             */
/*   Updated: 2019/04/05 20:43:01 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

void	ft_print_major_and_minor(t_list_ls **a, t_length *length)
{
	ft_print_space_i_num(length->max_size_major + 1, (*a)->size_major
	, (*a)->major);
	write(1, ", ", 2);
	ft_print_space_i_num(length->max_size_minor, (*a)->size_minor, (*a)->minor);
}

void	ft_print_m_j(t_list_ls **a, t_length *length)
{
	if ((*a)->access_right[0] == 'c' || (*a)->access_right[0] == 'b')
		ft_print_major_and_minor(a, length);
	else if (length->check_major == 1)
		ft_print_space_i_num(length->max_size_major + length->max_size_minor + 3
			, (*a)->length_size, (*a)->size);
	else
		ft_print_space_i_num(length->max_length_size, (*a)->length_size
		, (*a)->size);
	write(1, " ", 1);
	if (length->check_major && length->flag_bu)
		write(1, (*a)->last_file_mod, 4);
	else
		write(1, (*a)->last_file_mod, 12);
	write(1, " ", 1);
	write(1, (*a)->file_name, (*a)->length_name);
	if ((*a)->link)
	{
		write(1, " -> ", 4);
		write(1, (*a)->link, (*a)->link_length);
	}
}

void	ft_print_arg(t_list_ls **a, t_length *length)
{
	if ((*a)->check_error)
		return ;
	if (length->flag_a == 0 && (*a)->file_name[0] == '.')
		return ;
	if (length->flag_l == 1)
	{
		ft_print_acess_right(a, length);
		ft_print_space_i_num(length->max_link_caunt_size, (*a)->link_caunt_size
		, (*a)->link_caunt);
		write(1, " ", 1);
		if (!length->flag_g)
		{
			ft_print_space_i_array(length->max_length_pw, (*a)->length_pw
			, (*a)->pw_name);
			write(1, "  ", 2);
		}
		ft_print_space_i_array(length->max_length_gr, (*a)->length_gr
		, (*a)->gr_name);
		write(1, "  ", 2);
		ft_print_m_j(a, length);
		write(1, "\n", 1);
	}
}

void	ft_flag_1(t_list_ls *first_list, t_length *length)
{
	t_list_ls *list;

	list = first_list;
	if (length->flag_a == 0)
		while (list && (list->file_name[0] == '.'))
			list = list->next;
	while (list)
	{
		write(1, list->file_name, list->length_name);
		write(1, "\n", 1);
		list = list->next;
	}
}
