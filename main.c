/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aleksei <aleksei@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 17:34:10 by aleksei           #+#    #+#             */
/*   Updated: 2019/04/16 17:01:18 by hewisp           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

void	ft_check_no_arg_2(t_list_ls **new_list, t_length *length)
{
	if (!((*new_list) = (t_list_ls*)malloc(sizeof(t_list_ls)))
	|| !((*new_list)->file_name = ft_strdup("."))
	|| !((*new_list)->full_way_to_file = ft_strdup("./.")))
	{
		perror("ls");
		exit(0);
	}
	(*new_list)->length_name = 1;
	(*new_list)->next = NULL;
	(*new_list)->check_error = 0;
	if (length->flag_l)
	{
		ft_filling_structure(new_list, length);
		length->flag_a = 1;
		ft_apply_flag(new_list, length);
		ft_print_arg(new_list, length);
	}
	else
	{
		if (length->flag_t)
			ft_filling_structure(new_list, length);
		ft_print_no_flags((*new_list), length);
	}
}

int		ft_check_no_arg(int argc, char **argv, t_length *length)
{
	int			i;
	t_list_ls	*new_list;

	i = 1;
	new_list = NULL;
	while (i < argc)
	{
		if (argv[i])
			return (0);
		i++;
	}
	if (length->flag_d)
		ft_check_no_arg_2(&new_list, length);
	else
		ft_flag_r(length, "./");
	ft_free_list(&new_list, length);
	return (1);
}

void	ft_algorithm(int argc, char **argv, t_length *length)
{
	int			i;
	int			enter;

	i = 1;
	enter = 0;
	ft_parcing(argc, argv, length);
	if (ft_check_no_arg(argc, argv, length))
		return ;
	if (length->flag_d)
	{
		ft_flag_d(argc, argv, length);
		return ;
	}
	enter = ft_print_file(argc, argv, length);
	ft_print_dir(argc, argv, length, enter);
	ft_print_link(argc, argv, length);
}

int		main(int argc, char **argv)
{
	t_length	length;

	length.max_length_pw = 0;
	length.max_length_gr = 0;
	length.max_length_nm = 0;
	length.max_link_caunt_size = 0;
	length.max_length_size = 0;
	length.check_acl = 0;
	length.total_size = 0;
	length.max_size_major = 0;
	length.max_size_minor = 0;
	length.check_major = 0;
	ft_algorithm(argc, argv, &length);
	return (0);
}
